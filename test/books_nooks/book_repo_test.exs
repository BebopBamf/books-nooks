defmodule BooksNooks.BookRepoTest do
  use BooksNooks.DataCase

  alias BooksNooks.BookRepo

  describe "books" do
    alias BooksNooks.BookRepo.Book

    import BooksNooks.BookRepoFixtures

    @invalid_attrs %{description: nil, title: nil}

    test "list_books/0 returns all books" do
      book = book_fixture()
      assert BookRepo.list_books() == [book]
    end

    test "get_book!/1 returns the book with given id" do
      book = book_fixture()
      assert BookRepo.get_book!(book.id) == book
    end

    test "create_book/1 with valid data creates a book" do
      valid_attrs = %{description: "some description", title: "some title"}

      assert {:ok, %Book{} = book} = BookRepo.create_book(valid_attrs)
      assert book.description == "some description"
      assert book.title == "some title"
    end

    test "create_book/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = BookRepo.create_book(@invalid_attrs)
    end

    test "update_book/2 with valid data updates the book" do
      book = book_fixture()
      update_attrs = %{description: "some updated description", title: "some updated title"}

      assert {:ok, %Book{} = book} = BookRepo.update_book(book, update_attrs)
      assert book.description == "some updated description"
      assert book.title == "some updated title"
    end

    test "update_book/2 with invalid data returns error changeset" do
      book = book_fixture()
      assert {:error, %Ecto.Changeset{}} = BookRepo.update_book(book, @invalid_attrs)
      assert book == BookRepo.get_book!(book.id)
    end

    test "delete_book/1 deletes the book" do
      book = book_fixture()
      assert {:ok, %Book{}} = BookRepo.delete_book(book)
      assert_raise Ecto.NoResultsError, fn -> BookRepo.get_book!(book.id) end
    end

    test "change_book/1 returns a book changeset" do
      book = book_fixture()
      assert %Ecto.Changeset{} = BookRepo.change_book(book)
    end
  end
end
