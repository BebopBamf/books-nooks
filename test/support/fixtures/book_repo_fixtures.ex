defmodule BooksNooks.BookRepoFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `BooksNooks.BookRepo` context.
  """

  @doc """
  Generate a book.
  """
  def book_fixture(attrs \\ %{}) do
    {:ok, book} =
      attrs
      |> Enum.into(%{
        description: "some description",
        title: "some title"
      })
      |> BooksNooks.BookRepo.create_book()

    book
  end
end
