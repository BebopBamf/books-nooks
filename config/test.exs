import Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :books_nooks, BooksNooks.Repo,
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  database: "books_nooks_test#{System.get_env("MIX_TEST_PARTITION")}",
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :books_nooks, BooksNooksWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "Q8qGHJfyq+A460EUKPBfWIqSReir45UXiO9OdXU3X/HbtwCaBbkjL2ZG4zy4oV6l",
  server: false

# In test we don't send emails.
config :books_nooks, BooksNooks.Mailer, adapter: Swoosh.Adapters.Test

# Disable swoosh api client as it is only required for production adapters.
config :swoosh, :api_client, false

# Print only warnings and errors during test
config :logger, level: :warning

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
