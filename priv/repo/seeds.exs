# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     BooksNooks.Repo.insert!(%BooksNooks.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias BooksNooks.BookRepo.Book
alias BooksNooks.Repo

%Book{
  title: "The Great Gatsby",
  description: "He just wanted a decent book to read..."
}
|> Repo.insert!()

%Book{
  title: "Philosophical Writings",
  description: "Philosophy in"
}
|> Repo.insert!()

# BooksNooks.Repo.insert
