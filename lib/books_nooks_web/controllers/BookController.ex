defmodule BooksNooksWeb.BookController do
  alias BooksNooks.BookRepo

  def all_books(_root, _args, _info) do
    {:ok, BookRepo.list_books()}
  end
end
