defmodule BooksNooksWeb.Schema do
  use Absinthe.Schema

  alias BooksNooksWeb.BookController

  object :book do
    field :id, non_null(:id)
    field :title, non_null(:string)
    field :description, :string
  end

  query do
    @desc "Get a list of books"
    field :all_books, non_null(list_of(non_null(:book))) do
      resolve(&BookController.all_books/3)
    end
  end
end
