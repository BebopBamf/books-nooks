defmodule BooksNooks.Repo do
  use Ecto.Repo,
    otp_app: :books_nooks,
    adapter: Ecto.Adapters.Postgres
end
